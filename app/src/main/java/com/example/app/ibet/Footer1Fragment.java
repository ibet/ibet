package com.example.app.ibet;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.app.ibet.Activities.CompetitionsActivity;
import com.example.app.ibet.Activities.InfosActivity;
import com.example.app.ibet.Activities.LoginActivity;
import com.example.app.ibet.Activities.MybetsActivity;

public class Footer1Fragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    Button loginBtn,mybetsBtn,infosBtn,matchesBtn;
    Intent intent;
    private OnFragmentInteractionListener1 mListener;

    public Footer1Fragment() {
    }

    public static Footer1Fragment newInstance(String param1, String param2) {
        Footer1Fragment fragment = new Footer1Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_footer1, container, false);
        loginBtn = (Button)view.findViewById(R.id.login_btn);
        infosBtn = (Button)view.findViewById(R.id.infos_btn);
        matchesBtn = (Button)view.findViewById(R.id.matches_btn);
        mybetsBtn = (Button)view.findViewById(R.id.mybets_btn);

        loginBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });
        infosBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getActivity(), InfosActivity.class);
                startActivity(intent);

            }
        });
        matchesBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getActivity(), CompetitionsActivity.class);
                startActivity(intent);

            }
        });
        mybetsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //intent = new Intent(getActivity(), TestActivity.class);
                intent = new Intent(getActivity(), MybetsActivity.class);
                startActivity(intent);

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction1(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener1) {
            mListener = (OnFragmentInteractionListener1) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener1 {
        void onFragmentInteraction1(Uri uri);
    }
}
