package com.example.app.ibet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.app.ibet.Activities.CompetitionsActivity;
import com.example.app.ibet.Activities.InfosActivity;
import com.example.app.ibet.Activities.MainActivity;
import com.example.app.ibet.Activities.MybetsActivity;


public class Footer2Fragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    Button logoutBtn,mybetsBtn,infosBtn,matchesBtn;
    Intent intent;
    private OnFragmentInteractionListener2 mListener;

    public Footer2Fragment() {
    }


    public static Footer2Fragment newInstance(String param1, String param2) {
        Footer2Fragment fragment = new Footer2Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_footer2, container, false);
        logoutBtn = (Button)view.findViewById(R.id.logout_btn);
        infosBtn = (Button)view.findViewById(R.id.infos_btn);
        matchesBtn = (Button)view.findViewById(R.id.matches_btn);
        mybetsBtn = (Button)view.findViewById(R.id.mybets_btn);

        logoutBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                prefs.edit().putInt("login",0).commit();
                intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        infosBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getActivity(), InfosActivity.class);
                startActivity(intent);

            }
        });
        matchesBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getActivity(), CompetitionsActivity.class);
                startActivity(intent);

            }
        });
        mybetsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getActivity(), MybetsActivity.class);
                startActivity(intent);

            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction2(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener2) {
            mListener = (OnFragmentInteractionListener2) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener2 {
        // TODO: Update argument type and name
        void onFragmentInteraction2(Uri uri);
    }
}
