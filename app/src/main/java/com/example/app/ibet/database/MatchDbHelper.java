package com.example.app.ibet.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.app.ibet.Models.Match;

import java.util.ArrayList;

public class MatchDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 6;
    private static final String TABLE_NAME = "matche";
    private static final String ID ="id";
    private static final String MATCHID = "MATCHID";
    private static final String EQUIPEA ="EQUIPEA";
    private static final String EQUIPEB = "EQUIPEB";
    private static final String TEMPS = "TEMPS";
    private static final String BUTA = "BUTA";
    private static final String BUTB = "BUTB";
    private static final String DATE = "DATE";
    private static final String MATCH_STATUS = "MATCH_STATUS";
    private static final String LEAGUE_ID = "LEAGUE_ID";
    private static final String DATABASE_NAME = "matches.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    ID + "INTEGER PRIMARY KEY," +
                    MATCHID + " TEXT," +
                    EQUIPEA + " TEXT," +
                    EQUIPEB + " TEXT," +
                    TEMPS   + " TEXT," +
                    BUTA    + " TEXT," +
                    BUTB    + " TEXT," +
                    DATE    + " TEXT," +
                    MATCH_STATUS    + " TEXT," +
                    LEAGUE_ID + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public MatchDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        Log.d("matche","database created");
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db,oldVersion,newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void insert (String matchid,String equipea, String equipeb, String temps, String buta, String butb, String date,String match_status,String league_id) {
        Log.d("matche","invoke insert");
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MATCHID, matchid);
        values.put(EQUIPEA,equipea);
        values.put(EQUIPEB,equipeb);
        values.put(TEMPS,temps);
        values.put(BUTA,buta);
        values.put(BUTB,butb);
        values.put(DATE,date);
        values.put(MATCH_STATUS,match_status);
        values.put(LEAGUE_ID,league_id);

        db.insert(TABLE_NAME, null, values);
    }

    public ArrayList<Match> readMatchs() {
        Log.d("matche", "invoke read");
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(
                TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        ArrayList<Match> itemMatchs = new ArrayList<>();
        Match match ;
        while(cursor.moveToNext()) {
            String itemMatchid = cursor.getString(
                    cursor.getColumnIndexOrThrow(MATCHID));
            String itemEquipea = cursor.getString(
                    cursor.getColumnIndexOrThrow(EQUIPEA));
            String itemEquipeb = cursor.getString(
                    cursor.getColumnIndexOrThrow(EQUIPEB));
            String itemTemps = cursor.getString(
                    cursor.getColumnIndexOrThrow(TEMPS));
            String itemButa = cursor.getString(
                    cursor.getColumnIndexOrThrow(BUTA));
            String itemButb = cursor.getString(
                    cursor.getColumnIndexOrThrow(BUTB));
            String itemDate = cursor.getString(
                    cursor.getColumnIndexOrThrow(DATE));
            String itemStatus = cursor.getString(
                    cursor.getColumnIndexOrThrow(MATCH_STATUS));
            String itemLeagueid = cursor.getString(
                    cursor.getColumnIndexOrThrow(MATCH_STATUS));

            match = new Match(itemMatchid,itemEquipea,itemEquipeb,itemTemps,itemButa,itemButb,itemDate,itemStatus,itemLeagueid);
            itemMatchs.add(match);

        }
        cursor.close();
        return itemMatchs;
    }

    //Read one Match
    public Match getMatchById(int matchid)
    {
        Log.d("matche", "invoke read");
        Cursor cursor=null;
        Match match = null;
        SQLiteDatabase db = getReadableDatabase();
        cursor =  db.rawQuery("select * from " + TABLE_NAME + " where " + MATCHID + "=" + matchid  , null);
        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                int id = cursor.getInt(cursor.getColumnIndex(MATCHID));
                String equipea = cursor.getString(cursor.getColumnIndex(EQUIPEA));
                String equipeb = cursor.getString(cursor.getColumnIndex(EQUIPEB));
                String temps = cursor.getString(cursor.getColumnIndex(TEMPS));
                String buta = cursor.getString(cursor.getColumnIndex(BUTA));
                String butb = cursor.getString(cursor.getColumnIndex(BUTB));
                String date = cursor.getString(cursor.getColumnIndex(DATE));
                String match_status = cursor.getString(cursor.getColumnIndex(MATCH_STATUS));
                String league_id = cursor.getString(cursor.getColumnIndex(LEAGUE_ID));
                match = new Match(Integer.toString(id),equipea,equipeb,temps,buta,butb,date,match_status,league_id);
            }
            cursor.close();
        }
        return match;

    }

    public void updateResult(String matchid,String match_status,String butA, String butB) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MATCH_STATUS,match_status);
        contentValues.put(BUTA,butA);
        contentValues.put(BUTB,butB);
        db.update(TABLE_NAME, contentValues, "MATCHID = ?",new String[] { matchid });
    }
}
