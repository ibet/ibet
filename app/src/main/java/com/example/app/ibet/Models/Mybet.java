package com.example.app.ibet.Models;

public class Mybet {
    private String matchid;
    private String betchoice;
    private double gain;
    private String result;

    public Mybet(String matchid, String betchoice, double gain,String result) {
        this.matchid = matchid;
        this.betchoice = betchoice;
        this.gain = gain;
        this.result = result;
    }

    public String getMatch() {
        return matchid;
    }

    public void setMatch(String match) {
        this.matchid = match;
    }

    public String getBetchoice() {
        return betchoice;
    }

    public void setBetchoice(String betchoice) {
        this.betchoice = betchoice;
    }

    public double getGain() {
        return gain;
    }

    public void setGain(double gain) {
        this.gain = gain;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
