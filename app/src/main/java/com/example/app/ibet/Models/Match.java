package com.example.app.ibet.Models;

public class Match {
    private String match_id;
    private String equipeA;
    private String equipeB;
    private String temps;
    private String ButA;
    private String ButB;
    private String date;
    private String match_status ;
    private String league_id;
    private String match_live;

    public Match(String match_id,String equipeA, String equipeB, String temps, String butA, String butB, String date, String match_status,String league_id) {
        this.match_id = match_id;
        this.equipeA = equipeA;
        this.equipeB = equipeB;
        this.temps = temps;
        this.ButA = butA;
        this.ButB = butB;
        this.date = date;
        this.match_status = match_status;
        this.league_id = league_id;
    }

    public Match(String match_id,String equipeA, String equipeB, String temps, String butA, String butB, String date, String match_status,String league_id,String match_live) {
        this.match_id = match_id;
        this.equipeA = equipeA;
        this.equipeB = equipeB;
        this.temps = temps;
        this.ButA = butA;
        this.ButB = butB;
        this.date = date;
        this.match_status = match_status;
        this.league_id = league_id;
        this.match_live = match_live;
    }

    Match() {
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getEquipeA() {
        return equipeA;
    }

    public void setEquipeA(String equipeA) {
        this.equipeA = equipeA;
    }

    public String getEquipeB() {
        return equipeB;
    }

    public void setEquipeB(String equipeB) {
        this.equipeB = equipeB;
    }

    public String getTemps() {
        return temps;
    }

    public void setTemps(String temps) {
        this.temps = temps;
    }

    public String getButA() {
        return ButA;
    }

    public void setButA(String butA) {
        ButA = butA;
    }

    public String getButB() {
        return ButB;
    }

    public void setButB(String butB) {
        ButB = butB;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMatch_status() {
        return match_status;
    }

    public void setMatch_status(String match_status) {
        this.match_status = match_status;
    }

    public String getLeague_id() {
        return league_id;
    }

    public void setLeague_id(String league_id) {
        this.league_id = league_id;
    }

    public String getMatch_live() {
        return match_live;
    }

    public void setMatch_live(String match_live) {
        this.match_live = match_live;
    }
}
