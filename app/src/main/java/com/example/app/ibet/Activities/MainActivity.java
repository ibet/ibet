package com.example.app.ibet.Activities;

import com.example.app.ibet.Footer1Fragment;
import com.example.app.ibet.Footer2Fragment;
import com.example.app.ibet.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements Footer1Fragment.OnFragmentInteractionListener1, Footer2Fragment.OnFragmentInteractionListener2 {

    ImageView info;Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        info = (ImageView)findViewById(R.id.apropos);
        info.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, InfosActivity.class);
                startActivity(intent);

            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int login = prefs.getInt("login",0);

        if (login == 0 ) {
            Footer1Fragment footer = new Footer1Fragment();
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, footer).commit();
        } else if (login == 1) {
            Footer2Fragment footer = new Footer2Fragment();
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, footer).commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int login = prefs.getInt("login",0);

        if (login == 0 ) {
            Footer1Fragment footer = new Footer1Fragment();
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, footer).commit();
        } else if (login == 1) {
            Footer2Fragment footer = new Footer2Fragment();
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, footer).commit();
        }
    }

    @Override
    public void onFragmentInteraction1(Uri uri) {

    }

    @Override
    public void onFragmentInteraction2(Uri uri) {

    }
}
