package com.example.app.ibet.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.app.ibet.Models.Mybet;

import java.util.ArrayList;

public class MybetDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 6;
    private static final String TABLE_NAME = "mybet";
    private static final String ID = "id";
    private static final String MATCHID ="MATCHID";
    private static final String BETCHOICE = "BETCHOICE";
    private static final String GAIN = "GAIN";
    private static final String RESULT = "RESULT";
    private static final String DATABASE_NAME = "mybets.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    ID + "INTEGER PRIMARY KEY," +
                    MATCHID + " TEXT," +
                    BETCHOICE + " TEXT," +
                    RESULT + " TEXT," +
                    GAIN + " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public MybetDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        Log.d("mybet","database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db,oldVersion,newVersion);
    }

    public void insert (String matchid,String betchoice, String gain) {
        Log.d("mybet","invoke insert");
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MATCHID, matchid);
        values.put(BETCHOICE,betchoice);
        values.put(GAIN,gain);
        values.put(RESULT,"not yet");
        db.insert(TABLE_NAME, null, values);
    }


    public ArrayList<Mybet> readMybets() {
        Log.d("mybet", "invoke read");
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(
                TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        ArrayList<Mybet> itemMybets = new ArrayList<>();
        Mybet mybet ;
        while(cursor.moveToNext()) {
            String itemMatchid = cursor.getString(
                    cursor.getColumnIndexOrThrow(MATCHID));
            String itemBetchoice = cursor.getString(
                    cursor.getColumnIndexOrThrow(BETCHOICE));
            String itemGain = cursor.getString(
                    cursor.getColumnIndexOrThrow(GAIN));
            String itemResult = cursor.getString(
                    cursor.getColumnIndexOrThrow(RESULT));
            mybet = new Mybet(itemMatchid,itemBetchoice,Double.parseDouble(itemGain),itemResult);
            itemMybets.add(mybet);
        }
        cursor.close();
        return itemMybets;
    }

    //Read one Bet
    public Mybet getBetById(int matchid)
    {
        Log.d("mybet", "invoke read");
        Cursor cursor=null;
        Mybet match = null;
        SQLiteDatabase db = getReadableDatabase();
        cursor =  db.rawQuery("select * from " + TABLE_NAME + " where " + MATCHID + "=" + matchid  , null);
        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                int id = cursor.getInt(cursor.getColumnIndex(MATCHID));
                String betchoice = cursor.getString(cursor.getColumnIndex(BETCHOICE));
                String gain = cursor.getString(cursor.getColumnIndex(GAIN));
                String result = cursor.getString(cursor.getColumnIndex(RESULT));
                match = new Mybet(Integer.toString(id),betchoice,Double.parseDouble(gain),result);
            }
            cursor.close();
        }
        return match;

    }

    // Check if a bet exists
    public Boolean ifBetExists(String matchid) {
        Log.d("mybet", "invoke read");
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor;
        cursor =  db.rawQuery("select * from " + TABLE_NAME + " where " + MATCHID + "=" + matchid  , null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void updateResult(String matchid,String result) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(RESULT,result);
        db.update(TABLE_NAME, contentValues, "MATCHID = ?",new String[] { matchid });
    }

}