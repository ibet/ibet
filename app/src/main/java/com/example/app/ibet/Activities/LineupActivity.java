package com.example.app.ibet.Activities;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.app.ibet.R;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LineupActivity extends AppCompatActivity {

    private static final String BASE_API_URL = "https://apifootball.com/api";
    private static final String API_Key = "27a6c224848675a46d2a0c167b5b902538c196446f4dba5804dbe7d3e2fde142";
    String match_id,league_id,match_date;
    String homeaway;

    private OkHttpClient client = new OkHttpClient();

    TextView player1,player2,player3,player4,player5,player6,player7,player8,player9,player10,player11;

    String[] names = new String[11];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lineup);

        Bundle extras = getIntent().getExtras();

        match_id  = extras.getString("match_id");
        league_id  = extras.getString("league_id");
        match_date  = extras.getString("match_date");
        homeaway = extras.getString("homeaway");

        player1 = findViewById(R.id.player1);
        player2 = findViewById(R.id.player2);
        player3 = findViewById(R.id.player3);
        player4 = findViewById(R.id.player4);
        player5 = findViewById(R.id.player5);
        player6 = findViewById(R.id.player6);
        player7 = findViewById(R.id.player7);
        player8 = findViewById(R.id.player8);
        player9 = findViewById(R.id.player9);
        player10 = findViewById(R.id.player10);
        player11 = findViewById(R.id.player11);

        LineupActivity.MyAsyncTask myAsyncTask = new LineupActivity.MyAsyncTask();
        myAsyncTask.execute();
    }

    private void makeSyncCall() throws Exception {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(BASE_API_URL).newBuilder();
        urlBuilder.addQueryParameter("action", "get_events");
        urlBuilder.addQueryParameter("from", match_date);
        urlBuilder.addQueryParameter("to", match_date);
        urlBuilder.addQueryParameter("league_id", league_id);
        urlBuilder.addQueryParameter("match_id", match_id);
        urlBuilder.addQueryParameter("APIkey", API_Key);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder().url(url).build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                //throw new IOException("Unexpected code " + response);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alert("OUPS","Lineups for this match are not available yet");
                    }
                });
            }
            String myResponse = response.body().string();
            JSONArray json = new JSONArray(myResponse);
            JSONObject json2;
            JSONObject obj;
            String player_name;
            if(homeaway.equals("home")) {
                json2 = json.getJSONObject(0).getJSONObject("lineup").getJSONObject("home");
            } else {
                json2 = json.getJSONObject(0).getJSONObject("lineup").getJSONObject("away");
            }

            JSONArray json3 = json2.getJSONArray("starting_lineups");
            Log.d("json",json3.toString());
            if(json3.toString().equals("[]")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alert("Sorry","Lineups for this match are not available yet");
                    }
                });
            }
            else {
                for (int i = 0; i<json3.length(); i++){
                    obj = json3.getJSONObject(i);
                    player_name = obj.getString("lineup_player");
                    names[i] = splitname(player_name);
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    player1.setText(names[0]);
                    player2.setText(names[1]);
                    player3.setText(names[2]);
                    player4.setText(names[3]);
                    player5.setText(names[4]);
                    player6.setText(names[5]);
                    player7.setText(names[6]);
                    player8.setText(names[7]);
                    player9.setText(names[8]);
                    player10.setText(names[9]);
                    player11.setText(names[10]);
                }
            });
        }
    }

    private class MyAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                makeSyncCall();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void alert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(LineupActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        LineupActivity.this.finish();
                    }
                });
        alertDialog.show();
    }

    private String splitname (String name){
        return name.substring(name.lastIndexOf(" ")+1);
    }
}
