package com.example.app.ibet;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Button;

import com.example.app.ibet.Activities.LineupActivity;
import com.example.app.ibet.Activities.LoginActivity;
import com.example.app.ibet.Activities.MybetsActivity;
import com.example.app.ibet.database.MatchDbHelper;
import com.example.app.ibet.database.MybetDbHelper;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.GregorianCalendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.app.ibet.Utils.DateUtils.getTodayDate;

public class MatchDetails extends AppCompatActivity implements Footer1Fragment.OnFragmentInteractionListener1, Footer2Fragment.OnFragmentInteractionListener2 {

    private static final String BASE_API_URL = "https://apifootball.com/api";
    private static final String API_Key = "27a6c224848675a46d2a0c167b5b902538c196446f4dba5804dbe7d3e2fde142";
    private String MATCH_ID;

    private Button winA,winB,draw,betnow,lineup1,lineup2;
    private String betchoice,betcote1,betcote2,betcote3,betcotefinal,league_id,match_live;
    private OkHttpClient client = new OkHttpClient();

    private GregorianCalendar date = new GregorianCalendar();
    private String match_date;
    private EditText betprice;
    private TextView eqA,eqB,buA,buB,time,cote1,cote2,cote3;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_details);
        Bundle extras = getIntent().getExtras();

        mProgressBar = findViewById(R.id.progressBar);

        final MatchDbHelper matchDbHelper = new MatchDbHelper(this);
        final MybetDbHelper mybetDbHelper = new MybetDbHelper(this);

        MATCH_ID = extras.getString("match_id");
        final String equipeA  = extras.getString("equipeA");
        final String equipeB  = extras.getString("equipeB");
        final String temps  = extras.getString("temps");
        final String butA  = extras.getString("butA");
        final String butB  = extras.getString("butB");
        final String match_status = extras.getString("match_status");
        league_id = extras.getString("league_id");
        match_date = extras.getString("date");
        match_live = extras.getString("match_live");
        Log.d("live",match_live);
        if(match_live.equals("1")) {
            RelativeLayout relativeLayout1 = findViewById(R.id.choice);
            RelativeLayout relativeLayout2 = findViewById(R.id.cote);
            LinearLayout linearLayout = findViewById(R.id.bet);
            LinearLayout linearLayout2 = findViewById(R.id.message);

            relativeLayout1.setVisibility(View.GONE);
            relativeLayout2.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.VISIBLE);
        }

        String todayDate = getTodayDate();

        eqA = findViewById(R.id.eqA);
        eqA.setText(equipeA);

        eqB = findViewById(R.id.eqB);
        eqB.setText(equipeB);

        buA = findViewById(R.id.butA);
        buA.setText(butA);

        buB = findViewById(R.id.butB);
        buB.setText(butB);

        betprice = findViewById(R.id.betprice);

        time = findViewById(R.id.temps);
        if (!match_date.equals(todayDate)) {
            time.setText(match_date);
        } else {
            time.setText(temps);
        }

        cote1 = findViewById(R.id.cote1);
        cote2 = findViewById(R.id.cote2);
        cote3 = findViewById(R.id.cote3);

        winA = findViewById(R.id.winA);
        winB = findViewById(R.id.winB);
        draw = findViewById(R.id.draw);
        betnow = findViewById(R.id.betnow);
        betnow.setEnabled(false);

        lineup1 = findViewById(R.id.lineup1);
        lineup2 = findViewById(R.id.lineup2);

        winA.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                winA.setBackgroundColor(Color.rgb(0,125,0));
                winA.setTextColor(Color.WHITE);
                winB.setBackgroundColor(getResources().getColor(R.color.white));
                winB.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                draw.setBackgroundColor(getResources().getColor(R.color.white));
                draw.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                betchoice = "WinA";
                betcotefinal = betcote1;
                betnow.setEnabled(true);
            }
        });

        winB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                winB.setBackgroundColor(Color.rgb(0,125,0));
                winB.setTextColor(Color.WHITE);
                winA.setBackgroundColor(getResources().getColor(R.color.white));
                winA.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                draw.setBackgroundColor(getResources().getColor(R.color.white));
                draw.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                betchoice = "WinB";
                betcotefinal = betcote2;
                betnow.setEnabled(true);
            }
        });

        draw.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                draw.setBackgroundColor(Color.LTGRAY);
                draw.setTextColor(Color.WHITE);
                winA.setBackgroundColor(getResources().getColor(R.color.white));
                winA.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                winB.setBackgroundColor(getResources().getColor(R.color.white));
                winB.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                betchoice = "draw";
                betcotefinal = betcote3;
                betnow.setEnabled(true);
            }
        });

        betnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                int login = prefs.getInt("login",0);
                if (login == 0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(MatchDetails.this).create();
                    alertDialog.setTitle("SORRY");
                    alertDialog.setMessage("You need to be logged in first");
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(MatchDetails.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                    alertDialog.show();
                }

                String price = betprice.getText().toString();

                try {
                    double pricedouble = Double.parseDouble(price);
                    double betcotefinaldouble = Double.parseDouble(betcotefinal);
                    double gain = pricedouble * betcotefinaldouble;

                    // Check if the user already made a bet on this match
                    if (mybetDbHelper.ifBetExists(MATCH_ID)) {
                        alert("OUPS","You already bet on this  Match !");
                    } else {
                        matchDbHelper.insert(MATCH_ID,equipeA,equipeB,temps,butA,butB,match_date,match_status,league_id);
                        mybetDbHelper.insert(MATCH_ID,betchoice,Double.toString(gain));

                        Intent intent = new Intent(MatchDetails.this, MybetsActivity.class);
                        intent.putExtra("league_id",league_id);
                        startActivity(intent);
                        finish();
                    }

                } catch (NumberFormatException e) {
                    AlertDialog alertDialog = new AlertDialog.Builder(MatchDetails.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Please enter a valid amount");
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }

            }
        });

        redirect_lineup();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int login = prefs.getInt("login",0);

        if (login == 0 ) {
            Footer1Fragment footer = new Footer1Fragment();
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, footer).commit();
        } else if (login == 1) {
            Footer2Fragment footer = new Footer2Fragment();
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, footer).commit();
        }

        getOdds();
    }

    private void getOdds() {
        mProgressBar.setVisibility(View.VISIBLE);

        HttpUrl.Builder urlBuilder = HttpUrl.parse(BASE_API_URL).newBuilder();
        urlBuilder.addQueryParameter("action", "get_odds");
        urlBuilder.addQueryParameter("from", match_date);
        urlBuilder.addQueryParameter("to", match_date);
        urlBuilder.addQueryParameter("match_id", MATCH_ID);
        urlBuilder.addQueryParameter("APIkey", API_Key);
        String url = urlBuilder.build().toString();
        Log.d("URL",url);
        Request request = new Request.Builder().url(url).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alert("SORRY","Error connecting to the server .");
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                Boolean erreur = false;
                JSONArray json = null;
                String myResponse = response.body().string();
                try {
                    json = new JSONArray(myResponse);
                } catch (JSONException e) {
                    erreur = true;
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.GONE);
                            betnow.setEnabled(false);
                            alert("SORRY","Odds not yet available for this match , please try later .");
                            betprice.setFocusable(false);
                            betnow.setEnabled(false);
                            winA.setEnabled(false);
                            winB.setEnabled(false);
                            draw.setEnabled(false);
                        }
                    });
                }
                String cote1value = null;
                String cote2value = null;
                String cote3value = null;
                if (!erreur) {
                    try {
                        cote1value = json.getJSONObject(0).getString("odd_1");
                        cote2value = json.getJSONObject(0).getString("odd_x");
                        cote3value = json.getJSONObject(0).getString("odd_2");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alert("SORRY","Please try again later");
                            }
                        });
                    }
                }
                final String finalCote1value = cote1value;
                final String finalCote2value = cote2value;
                final String finalCote3value = cote3value;
                betcote1 = finalCote1value;
                betcote2 = finalCote2value;
                betcote3 = finalCote3value;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cote1.setText(finalCote1value);
                        cote2.setText(finalCote2value);
                        cote3.setText(finalCote3value);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
    }


    private void alert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(MatchDetails.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void redirect_lineup(){
        lineup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MatchDetails.this, LineupActivity.class);
                intent.putExtra("league_id",league_id);
                intent.putExtra("match_id",MATCH_ID);
                intent.putExtra("from",match_date);
                intent.putExtra("to",match_date);
                intent.putExtra("homeaway","home");
                startActivity(intent);
            }
        });
        lineup2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MatchDetails.this, LineupActivity.class);
                intent.putExtra("league_id",league_id);
                intent.putExtra("match_id",MATCH_ID);
                intent.putExtra("from",match_date);
                intent.putExtra("to",match_date);
                intent.putExtra("homeaway","away");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onFragmentInteraction1(Uri uri) {

    }

    @Override
    public void onFragmentInteraction2(Uri uri) {

    }
}
