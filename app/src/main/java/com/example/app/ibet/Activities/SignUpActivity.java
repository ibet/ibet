package com.example.app.ibet.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.ibet.Helpers.InputValidation;
import com.example.app.ibet.Models.User;
import com.example.app.ibet.R;
import com.example.app.ibet.database.UserDbHelper;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView info;

    private final AppCompatActivity activity = SignUpActivity.this;

    private Button register_btn;

    private TextView textview;
    private TextView textview2;
    private TextView textview3;

    private EditText nom;
    private EditText password;
    private EditText confirm_Password;

    private AppCompatButton appCompatButtonRegister;
    private AppCompatTextView appCompatTextViewLoginLink;

    private InputValidation inputValidation;
    private UserDbHelper userDbHelper;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initViews();
        initListeners();
        initObjects();
    }

    private void initViews() {
        textview = (TextView) findViewById(R.id.textView);
        textview2 = (TextView) findViewById(R.id.textView2);
        textview3 = (TextView) findViewById(R.id.textView3);

        nom = (EditText) findViewById(R.id.nom);
        password = (EditText) findViewById(R.id.password);
        confirm_Password = (EditText) findViewById(R.id.confirmpassword);

        register_btn = (Button) findViewById(R.id.signup_btn);

        appCompatTextViewLoginLink = (AppCompatTextView) findViewById(R.id.appCompatTextViewLoginLink);

    }

    private void initListeners() {
        register_btn.setOnClickListener(this);
        appCompatTextViewLoginLink.setOnClickListener(this);

    }


    private void initObjects() {
        inputValidation = new InputValidation(activity);
        userDbHelper = new UserDbHelper(activity);
        user = new User();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.signup_btn:
                postDataToSQLite();
                break;

            case R.id.appCompatTextViewLoginLink:
                Intent intentLogin = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intentLogin);
                finish();
                break;
        }
    }

    private void postDataToSQLite() {
        if (!inputValidation.isInputEditTextFilled(nom, textview)) {
            Toast.makeText(getApplicationContext(),"Please enter your username",Toast.LENGTH_SHORT).show();
            return;
        }
        if (!inputValidation.isInputEditTextFilled(password, textview2)) {
            Toast.makeText(getApplicationContext(),"Please enter your password",Toast.LENGTH_SHORT).show();
            return;
        }
        if (!inputValidation.isInputEditTextMatches(password,confirm_Password, textview3)) {
            Toast.makeText(getApplicationContext(),"Passwords don't match",Toast.LENGTH_SHORT).show();
            return;
        }

        if (!userDbHelper.checkUser(nom.getText().toString().trim())) {

            user.setName(nom.getText().toString().trim());
            user.setPassword(password.getText().toString().trim());

            userDbHelper.addUser(user);
            Toast.makeText(getApplicationContext(),"You've been registred",Toast.LENGTH_SHORT).show();
            emptyInputEditText();
            return;


        } //else {
        //}
    }

    private void emptyInputEditText() {
        nom.setText(null);
        password.setText(null);
        confirm_Password.setText(null);
    }
}
