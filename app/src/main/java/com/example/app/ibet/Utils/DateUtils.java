package com.example.app.ibet.Utils;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class DateUtils {

    public static String getTodayDate() {
        GregorianCalendar date = new GregorianCalendar();
        return format(date);
    }

    public static String format(GregorianCalendar calendar){
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        fmt.setCalendar(calendar);
        return fmt.format(calendar.getTime());
    }
}
