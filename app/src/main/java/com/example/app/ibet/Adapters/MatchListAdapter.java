package com.example.app.ibet.Adapters;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.example.app.ibet.Models.Match;
import com.example.app.ibet.R;

import static com.example.app.ibet.Utils.DateUtils.getTodayDate;


public class MatchListAdapter extends ArrayAdapter{

    private ArrayList<Match> matches;
    private String todayDate = getTodayDate();

    public MatchListAdapter(Context context, int resource, ArrayList<Match> matches) {
        super(context, resource, matches);
        this.matches = matches;
    }

    @NonNull
    @Override
    public View getView(int position,View convertView,ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.match,parent,false);
        TextView equipe1 = (TextView) convertView.findViewById(R.id.equipe1);
        equipe1.setText(matches.get(position).getEquipeA());
        TextView equipe2 = (TextView) convertView.findViewById(R.id.equipe2);
        equipe2.setText(matches.get(position).getEquipeB());
        TextView but1 = (TextView) convertView.findViewById(R.id.but1);
        but1.setText(matches.get(position).getButA());
        TextView but2 = (TextView) convertView.findViewById(R.id.but2);
        but2.setText(matches.get(position).getButB());
        TextView temps = (TextView) convertView.findViewById(R.id.temps);
        temps.setText(matches.get(position).getTemps());
        TextView date = (TextView) convertView.findViewById(R.id.date);
        date.setText(matches.get(position).getDate());
        TextView live = (TextView) convertView.findViewById(R.id.live);
        CardView myCard = (CardView) convertView.findViewById(R.id.matchcard);
        if(matches.get(position).getMatch_live().equals("1")) {
            live.setText("Live");
            live.setTextColor(Color.GREEN);
            myCard.setBackgroundResource(R.drawable.border_orange);
        }
        else{
            myCard.setBackgroundResource(R.drawable.border_grey);
        }

        return convertView;
    }
}
