package com.example.app.ibet.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.app.ibet.Models.Match;
import com.example.app.ibet.Models.Mybet;
import com.example.app.ibet.R;
import com.example.app.ibet.database.MatchDbHelper;
import com.example.app.ibet.database.MybetDbHelper;

import java.util.ArrayList;

public class MybetListAdapter extends ArrayAdapter{

    private ArrayList<Mybet> mybets;

    public MybetListAdapter(Context context, int resource, ArrayList<Mybet> mybets) {
        super(context, resource, mybets);
        this.mybets = mybets;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MatchDbHelper matchDbHelper = new MatchDbHelper(getContext());
        MybetDbHelper mybetDbHelper = new MybetDbHelper(getContext());
        int matchid = Integer.parseInt(mybets.get(position).getMatch());
        Match match = matchDbHelper.getMatchById(matchid);
        Mybet mybet = mybetDbHelper.getBetById(matchid);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.mybet,parent,false);

        TextView equipe1 = convertView.findViewById(R.id.equipe1);
        equipe1.setText(match.getEquipeA());

        TextView equipe2 = convertView.findViewById(R.id.equipe2);
        equipe2.setText(match.getEquipeB());

        TextView but1 = convertView.findViewById(R.id.but1);
        but1.setText(match.getButA());

        TextView but2 = convertView.findViewById(R.id.but2);
        but2.setText(match.getButB());

        TextView yourbet = convertView.findViewById(R.id.yourbet);
        yourbet.setText("Your Bet : "+mybets.get(position).getBetchoice());

        TextView result = convertView.findViewById(R.id.result);

        CardView mCardView = convertView.findViewById(R.id.cardview);
        if (mybet.getResult().equals("win")) {
            result.setText("You Won "+Double.toString(mybets.get(position).getGain())+"$");
            result.setTextColor(getContext().getResources().getColor(R.color.green));
            mCardView.setBackgroundResource(R.drawable.border_green);
        }
        if (mybet.getResult().equals("lose")) {
            result.setText("You Lost");
            result.setTextColor(getContext().getResources().getColor(R.color.red));
            mCardView.setBackgroundResource(R.drawable.border_red);
        }
        if (mybet.getResult().equals("not yet")) {
            result.setText("Not Yet");
        }

        return convertView;
    }
}