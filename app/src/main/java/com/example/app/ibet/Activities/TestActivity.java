package com.example.app.ibet.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.app.ibet.Models.Match;
import com.example.app.ibet.Models.Mybet;
import com.example.app.ibet.R;
import com.example.app.ibet.database.MatchDbHelper;
import com.example.app.ibet.database.MybetDbHelper;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {
    private static final String BASE_API_URL = "https://apifootball.com/api";
    private static final String API_Key = "27a6c224848675a46d2a0c167b5b902538c196446f4dba5804dbe7d3e2fde142";
    private String MATCH_ID;
    private String match_status;
    private String match_date;
    private String league_id;
    private String match_hometeam_score;
    private String match_awayteam_score;
    private MybetDbHelper mybetDbHelper = new MybetDbHelper(this);
    private MatchDbHelper matchDbHelper = new MatchDbHelper(this);
    private ArrayList<Mybet> mybets ;
    private Match match ;

    TextView mTextView;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mTextView = findViewById(R.id.textviewtest);

        ArrayList<Match> matches = matchDbHelper.readMatchs();
        for (Match match : matches) {
            matchDbHelper.updateResult(match.getMatch_id(),"","-","-");
        }
    }


}
