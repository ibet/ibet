package com.example.app.ibet.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.ibet.Helpers.InputValidation;
import com.example.app.ibet.R;
import com.example.app.ibet.database.UserDbHelper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    Button log;
    ImageView info;
    TextView signup;

    private final AppCompatActivity activity = LoginActivity.this;



    private EditText nom;
    private EditText password;

    private TextView textview;
    private TextView textView2;

    private AppCompatTextView textViewLinkRegister;

    private InputValidation inputValidation;
    private UserDbHelper userDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    private void initViews() {

        nom = (EditText)findViewById(R.id.nom);
        password = (EditText) findViewById(R.id.password);

        textview = (TextView)findViewById(R.id.textView);
        textView2 = (TextView)findViewById(R.id.textView);

        log = (Button) findViewById(R.id.loginBtn);

        textViewLinkRegister = (AppCompatTextView) findViewById(R.id.textViewLinkRegister);

    }

    private void initListeners() {
        log.setOnClickListener(this);
        textViewLinkRegister.setOnClickListener(this);
    }


    private void initObjects() {
        userDbHelper = new UserDbHelper(activity);
        inputValidation = new InputValidation(activity);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                verifyFromSQLite();

                break;
            case R.id.textViewLinkRegister:
                Intent intentSignup = new Intent(this, SignUpActivity.class);
                startActivity(intentSignup);
                finish();
                break;
        }
    }

    private void verifyFromSQLite() {
        if (!inputValidation.isInputEditTextFilled(nom, textview)) {
            Toast.makeText(getApplicationContext(),"Please enter your username",Toast.LENGTH_SHORT).show();
            return;
        }
        if (!inputValidation.isInputEditTextFilled(password, textView2)) {
            Toast.makeText(getApplicationContext(),"Please enter your password",Toast.LENGTH_SHORT).show();
            return;
        }

        if (userDbHelper.checkUser(nom.getText().toString().trim()
                , password.getText().toString().trim())) {

            Intent mainIntent = new Intent(activity, MainActivity.class);
            mainIntent.putExtra("USERNAME", nom.getText().toString().trim());
            emptyInputEditText();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            prefs.edit().putInt("login",1).commit();
            startActivity(mainIntent);
            finish();

        } else {
            Toast.makeText(getApplicationContext(),"Cannot log you in",Toast.LENGTH_SHORT).show();
        }
    }


    private void emptyInputEditText() {
        nom.setText(null);
        password.setText(null);
    }
}
