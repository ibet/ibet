package com.example.app.ibet.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.app.ibet.MatchDetails;
import com.example.app.ibet.Adapters.MatchListAdapter;
import com.example.app.ibet.Models.Match;
import com.example.app.ibet.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.app.ibet.Utils.DateUtils.format;
import static com.example.app.ibet.Utils.DateUtils.getTodayDate;

public class MatchesActivity extends AppCompatActivity {

    private static final String BASE_API_URL = "https://apifootball.com/api";
    private static final String API_Key = "27a6c224848675a46d2a0c167b5b902538c196446f4dba5804dbe7d3e2fde142";
    private String LEAGUE_ID ;
    private ListView liste;
    private ArrayList<Match> match;
    private OkHttpClient client;

    private GregorianCalendar date = new GregorianCalendar();
    private String todayDate ;
    private String twoDaysAfterDate ;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);

        mProgressBar = findViewById(R.id.progressBar);
        LEAGUE_ID = getIntent().getStringExtra("league_id");

        todayDate = getTodayDate();
        date.add(Calendar.DATE, 8);
        twoDaysAfterDate = format(date);

        liste = findViewById(R.id.liste_match);

        client = new OkHttpClient();

        match = new ArrayList<>();
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute();
    }

    private class MyAsyncTask extends AsyncTask<Void,Void,Void> {

        String match_id;
        String match_hometeam_name;
        String match_awayteam_name;
        String match_time;
        String match_hometeam_score;
        String match_awayteam_score;
        String match_date;
        String match_status;
        String match_live;

        Boolean erreur=false;

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            HttpUrl.Builder urlBuilder = HttpUrl.parse(BASE_API_URL).newBuilder();
            urlBuilder.addQueryParameter("action", "get_events");
            urlBuilder.addQueryParameter("from", todayDate);
            urlBuilder.addQueryParameter("to", twoDaysAfterDate);
            urlBuilder.addQueryParameter("league_id", LEAGUE_ID);
            urlBuilder.addQueryParameter("APIkey", API_Key);
            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();

            try (Response response = client.newCall(request).execute()) {

                String myResponse = response.body().string();
                JSONArray json = new JSONArray(myResponse);
                Match match_response;
                for (int i = 0; i<json.length(); i++) {
                    match_id = json.getJSONObject(i).getString("match_id");
                    match_hometeam_name = json.getJSONObject(i).getString("match_hometeam_name");
                    match_awayteam_name = json.getJSONObject(i).getString("match_awayteam_name");
                    match_time = json.getJSONObject(i).getString("match_time");
                    match_date = json.getJSONObject(i).getString("match_date");
                    match_hometeam_score = json.getJSONObject(i).getString("match_hometeam_score");
                    match_live = json.getJSONObject(i).getString("match_live");
                    if (match_hometeam_score.equals(""))
                        match_hometeam_score = "-";
                    match_awayteam_score = json.getJSONObject(i).getString("match_awayteam_score");
                    if (match_awayteam_score.equals(""))
                        match_awayteam_score = "-";
                    match_status = json.getJSONObject(i).getString("match_status");
                    match_response = new Match(match_id,match_hometeam_name, match_awayteam_name, match_time, match_hometeam_score,
                            match_awayteam_score,match_date,match_status,LEAGUE_ID,match_live);
                    match.add(match_response);
                }
            } catch (JSONException | IOException e) {
                //e.printStackTrace();
                erreur=true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (erreur) {
                alert("No Matches Availables","Please come back later" );

            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(View.GONE);
                }
            });

            MatchListAdapter adapter = new MatchListAdapter(getApplicationContext(), R.layout.match, match);
            liste.setAdapter(adapter);

            liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                    Intent intent;
                    intent = new Intent(MatchesActivity.this, MatchDetails.class);
                    intent.putExtra("match_id",match.get(position).getMatch_id());
                    intent.putExtra("equipeA", match.get(position).getEquipeA());
                    intent.putExtra("equipeB", match.get(position).getEquipeB());
                    intent.putExtra("temps", match.get(position).getTemps());
                    intent.putExtra("butA", match.get(position).getButA());
                    intent.putExtra("butB", match.get(position).getButB());
                    intent.putExtra("date",match.get(position).getDate());
                    intent.putExtra("league_id",LEAGUE_ID);
                    intent.putExtra("match_status",match.get(position).getMatch_status());
                    intent.putExtra("match_live",match.get(position).getMatch_live());
                    startActivity(intent);
                }
            });
        }
    }

    private void alert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(MatchesActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.show();
    }
}
