package com.example.app.ibet.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.app.ibet.Models.Match;
import com.example.app.ibet.Models.Mybet;
import com.example.app.ibet.Adapters.MybetListAdapter;
import com.example.app.ibet.R;
import com.example.app.ibet.database.MatchDbHelper;
import com.example.app.ibet.database.MybetDbHelper;

import org.json.JSONArray;
import java.util.ArrayList;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MybetsActivity extends AppCompatActivity {
    private static final String BASE_API_URL = "https://apifootball.com/api";
    private static final String API_Key = "27a6c224848675a46d2a0c167b5b902538c196446f4dba5804dbe7d3e2fde142";
    private String MATCH_ID;
    private OkHttpClient client = new OkHttpClient();
    private String match_status;
    private String match_date;
    private String league_id;
    private String match_hometeam_score;
    private String match_awayteam_score;
    private boolean didUserWin = false;
    private int scorea,scoreb;

    private ArrayList<Mybet> mybets;
    private ListView liste;
    MybetDbHelper mybetDbHelper = new MybetDbHelper(this);
    MatchDbHelper matchDbHelper = new MatchDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mybets);
        liste = findViewById(R.id.liste_bets);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int login = prefs.getInt("login",0);
        if (login == 0 ) {
            AlertDialog alertDialog = new AlertDialog.Builder(MybetsActivity.this).create();
            alertDialog.setTitle("SORRY");
            alertDialog.setMessage("You need to be logged in first !");
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(MybetsActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
            alertDialog.show();
        }

        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute();
    }

    private void makeSyncCall() throws Exception {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(BASE_API_URL).newBuilder();
        urlBuilder.addQueryParameter("action", "get_events");
        urlBuilder.addQueryParameter("from", match_date);
        urlBuilder.addQueryParameter("to", match_date);
        urlBuilder.addQueryParameter("league_id", league_id);
        urlBuilder.addQueryParameter("match_id", MATCH_ID);
        urlBuilder.addQueryParameter("APIkey", API_Key);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder().url(url).build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                //throw new IOException("Unexpected code " + response);
                alert("ERROR","Please try again later");
            }
            String myResponse = response.body().string();
            JSONArray json = new JSONArray(myResponse);

            for (int i = 0; i<json.length(); i++) {
                match_status = json.getJSONObject(i).getString("match_status");
                match_hometeam_score = json.getJSONObject(i).getString("match_hometeam_score");
                match_awayteam_score = json.getJSONObject(i).getString("match_awayteam_score");
                scorea = Integer.parseInt(match_hometeam_score);
                scoreb = Integer.parseInt(match_awayteam_score);
            }
        }
    }
    private class MyAsyncTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            mybets = mybetDbHelper.readMybets();
            if (mybets.size() == 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alert("Oups","You don't have any bet yet !");
                    }
                });
            }

            for(Mybet mybet : mybets){
                MATCH_ID = mybet.getMatch();
                Match match = matchDbHelper.getMatchById(Integer.parseInt(MATCH_ID));
                match_date = match.getDate();
                league_id = match.getLeague_id();
                match_status = match.getMatch_status();

                if (!match_status.equals("FT")) {
                    try {
                        makeSyncCall();
                    } catch (Exception e) {
                        //e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //alert("ERROR","Please try again later");
                            }
                        });
                    }
                    if (match_status.equals("FT")) {
                        //Score
                        switch (mybet.getBetchoice()) {
                            case "WinA":
                                if (scorea > scoreb) {
                                    didUserWin = true;
                                }
                                break;
                            case "WinB":
                                if (scoreb > scorea) {
                                    didUserWin = true;
                                }
                                break;
                            default:
                                if (scorea == scoreb) {
                                    didUserWin = true;
                                }
                                break;
                        }
                        if (didUserWin) {
                            mybetDbHelper.updateResult(MATCH_ID, "win");
                        } else {
                            mybetDbHelper.updateResult(MATCH_ID,"lose");
                        }
                        matchDbHelper.updateResult(MATCH_ID,match_status,match_hometeam_score,match_awayteam_score);
                    }
                }
                didUserWin = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mybets = mybetDbHelper.readMybets();
            MybetListAdapter adapter = new MybetListAdapter(getApplicationContext(), R.layout.mybet, mybets);
            liste.setAdapter(adapter);
        }
    }

    private void alert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(MybetsActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}