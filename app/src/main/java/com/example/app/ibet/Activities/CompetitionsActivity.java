package com.example.app.ibet.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.app.ibet.R;

public class CompetitionsActivity extends AppCompatActivity {
    ImageView epl,ligue2;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competitions);
        epl = (ImageView) findViewById(R.id.epl_league);
        ligue2 = (ImageView) findViewById(R.id.ligue2);
        epl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(CompetitionsActivity.this, MatchesActivity.class);
                intent.putExtra("league_id","63");
                startActivity(intent);

            }
        });
        ligue2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(CompetitionsActivity.this, MatchesActivity.class);
                intent.putExtra("league_id","128");
                startActivity(intent);

            }
        });
    }
}
