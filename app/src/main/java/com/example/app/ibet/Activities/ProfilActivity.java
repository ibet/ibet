package com.example.app.ibet.Activities;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.app.ibet.Footer1Fragment;
import com.example.app.ibet.Footer2Fragment;
import com.example.app.ibet.R;

public class ProfilActivity extends AppCompatActivity implements Footer1Fragment.OnFragmentInteractionListener1, Footer2Fragment.OnFragmentInteractionListener2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        Footer1Fragment footer = new Footer1Fragment();
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, footer).commit();
    }

    @Override
    public void onFragmentInteraction1(Uri uri) {

    }

    @Override
    public void onFragmentInteraction2(Uri uri) {

    }
}